package view;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

import com.sun.xml.internal.bind.v2.runtime.reflect.Lister.IDREFSIterator;

import controller.Controller;
import model.data_structures.IList;
import model.vo.VORoute;

import model.vo.VOStop;
import sun.security.x509.IssuingDistributionPointExtension;

import java.util.Date;
public class STSManagerView {


	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
        boolean fin = false;
        int service=-1;
        while (!fin) {
            printMenu();

            int option = sc.nextInt();
            switch (option) {
                //1C
                case 1:

                    //Memoria y tiempo
                    long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
                    long startTime = System.nanoTime();

                    //Cargar data
                    Controller.ITScargarGTFS();

                    //Tiempo en cargar
                    long endTime = System.nanoTime();
                    long duration = (endTime - startTime) / (1000000);

                    //Memoria usada
                    long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
                    System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  " + ((memoryAfterCase1 - memoryBeforeCase1) / 1000000.0) + " MB");

                    break;

                //1C
                case 2:
                    System.out.println("Ingrese la fecha deseada Ej: 20170625 (AnoMesDia) \n Esta fecha se utilizara para los otros metodos.");

                    //Fecha deseada
    				 String fechaCase2 = sc.next();
    				 
    				
				System.out.println("La fecha dada fue "+fechaCase2);
					  final Calendar c = Calendar.getInstance();
						SimpleDateFormat sdff = new SimpleDateFormat("yyyyMMdd");
						  final Calendar cc = Calendar.getInstance();
					         try {
					             cc.setTime(sdff.parse(fechaCase2));
					             int mes=cc.get(Calendar.MONTH);
					             int dia =cc.get(Calendar.DAY_OF_MONTH);
				                  service=cc.get(Calendar.DAY_OF_WEEK) ; 
					             long memoryBeforeCase2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				                 long startTime2 = System.nanoTime();					             
					             
				                 //Controller metodo
				                 
					             
					             long endTime2 = System.nanoTime();
				                 long duration2 = (endTime2 - startTime2) / (1000000);

				                    //Memoria usada
				                    long memoryAfterCase2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				                    System.out.println("Tiempo en cargar: " + duration2 + " milisegundos \nMemoria utilizada:  " + ((memoryAfterCase2 - memoryBeforeCase2) / 1000000.0) + " MB");
					         } 
					         catch (ParseException e) {
					             System.out.println("Formato inv�lido de la fecha "+e);
					         }
					         					         
                    break;
                    
                //1A
                case 3:

                    //id de la ruta
                    System.out.println("Ingrese el id de la ruta deseada");
                    String idRouteCase3 = sc.next();
                    
                	System.out.println("Ingrese la fecha que desea EJ 20170821" );
                	String f = sc.next();
                    
                    break;

                //2A
                case 4:
 //fecha
                    //n
                    System.out.println("Ingrese el n deseado: (n>0)");
                    String nCase4 = sc.next();	
                	System.out.println("Ingrese la fecha que desea EJ 20170821" );
                	String a = sc.next();


                    break;

                //3A
                case 5:

                    //id de la ruta
                    System.out.println("Ingrese el id de la ruta deseada");
                    String idRouteCase5 = sc.next();

             //       System.out.println("Ingrese la fecha que desea EJ 20170821" );
               // 	String a5 = sc.next();
                	

                    break;

                //1B
                case 6:

                    //Id ruta
                    System.out.println("Ingrese el id de la ruta deseada");
                    String idRutaCase6 = sc.next();
                    int routeId6 = Integer.parseInt(idRutaCase6);

                    
                    break;

                //2B
                case 7:
                    //Id ruta
                    System.out.println("Ingrese el id de la ruta deseada");
                    String idRutaCase7 = sc.next();
                    int routeId7 =Integer.parseInt(idRutaCase7);

                    
                    break;

                //3B
                case 8:

                    //Id parada origen
                    System.out.println("Ingrese el id de la parada origen");
                    String idOrigen8 = sc.next();
                    int stopOr=Integer.parseInt(idOrigen8);
                    
                    //Id parada destino
                    System.out.println("Ingrese el id de la parada destino");
                    String idDestino8 = sc.next();
                    int stopDes=Integer.parseInt(idDestino8);
                    
                    //Hora de inicio
                    System.out.println("Ingrese la hora inicial en formato 23:50");
                    String horaInicio8 = sc.next();
                    
                    //Hora de fin
                    System.out.println("Ingrese la hora final en formato 23:50");
                    String horaFin8 = sc.next();
                    //TODO REQUERIMIENTO 3B
					/**
					 * Metodo de Controller
					 * @param String idOrigen
					 * @param String idDestino
					 * @param String horaInicio
					 * @param String horaFin
					 * @return IList<VOViaje>
					 */
                    break;

                //2C
                case 9:
                	System.out.println("Ingrese la fecha que desea EJ 20170821" );
                	String aa = sc.next();
                    System.out.println("Ingrese el n que desea:" );
                	String b = sc.next();
                	int bb=Integer.parseInt(b);

                    break;

                //3C
                case 10:

                    //Id viaje
                    System.out.println("Ingrese el id del viaje deseado:");
                    String idViajeCase10 = sc.next();
                    System.out.println("el viaje dado fue " +idViajeCase10);
                    System.out.println("Ingrese la fecha que desea EJ 20170821" );
                    String fecha1 = sc.next();
                    System.out.println("La fecha dada fue " +fecha1);
                    int trip=Integer.parseInt(idViajeCase10);
                    
                 
                    break;

                //4C
                case 11:

                    //Id parada
                    System.out.println("Ingerse el id de la parada deseada");
                    String idParada11 = sc.next();
                    int stopId=Integer.parseInt(idParada11);
			
					// TODO Auto-generated catch block
				


                    break;

                //5C
                case 12:

                    //Ruta
                    System.out.println("Ingrese el id de la ruta deseada");
                    String rutaCase12 = sc.next();
                    int routeId = Integer.parseInt(rutaCase12);

                    //HoraInicio
                    System.out.println("Ingrese la hora inicial en formato 23:50 ");
                    String horaInicio12 = sc.next();

                    //HoraFinal
                    System.out.println("Ingrese la hora final en formato 23:50 ");
                    String horaFinal12 = sc.next();

                    // REQUERIMIENTO 5C
                    
                    break;


                //SALIR
                case 13:

                    fin = true;
                    sc.close();
                    break;

            }
        }
	}

        /**
         * Menu
         */

    private static void printMenu() {
        System.out.println("---------ISIS 1206 - Estructuras de datos----------");
        System.out.println("---------------------Proyecto 2----------------------");
        System.out.println("Cargar data (1C):");
        System.out.println("1. Cargar la informaci�n est�tica necesaria para la operaci�n del sistema");
        System.out.println("2. Cargar la informaci�n en tiempo real de los buses dada una fecha\n");

        System.out.println("Parte A:");
        System.out.println("3.(1A) Dada una ruta  y una fecha,  identificar todos los viajes en los que hubo un retardo en al menos una parada.");
        System.out.println("4.(2A) Identificar  las n paradas en las que hubo más retardos en una fecha dada, teniendo en cuenta todas las rutas que utilizan esa parada.");
        System.out.println("5.(3A) Para una ruta dada en una fecha dada, retornar una lista ordenada  (por tiempo total de viaje) de todos los transbordos posibles, a  otras rutas, a partir de las paradas de dicha ruta.  \n");


        System.out.println("Parte B: ");
        System.out.println("6.(1B) Dada una ruta y una fecha, identificar todos los  viajes en los que después  de un retardo, todas las paradas siguientes tuvieron retardo. Retornar lista ordenada descendentemente por id de viaje.");
        System.out.println("7.(2B) Identificar  la franja de hora entera en la que hubo más paradas con retardos, para una ruta determinada en una fecha dada.");
        System.out.println("8.(3B) Dados los ids de una parada de origen y una parada de destino, buscar los viajes de rutas de bus para ir del origen al destino, en una fecha y franja de horario.  \n");


        System.out.println("Parte C:");
        System.out.println("9.(2C) Identificar  los n viajes que recorren más distancia  en una fecha dada, retirnando una lista ordenada por esta distancia.");
        System.out.println("10.(3C) Retornar un árbol  balanceado de retardos de un viaje (ordenado por tiempo de retardo), dado su  identificador y una fecha  dada. ");
        System.out.println("11.(4C) Dada una parada y una fecha, responder si dicha parada es compartida o no en dicha fecha.");
        System.out.println("12.(5C) Dada una ruta, dar todos los viajes (con sus respectivas paradas) que realizaron paradas en un rango de tiempo dado. \n");
        System.out.println("13. Salir.\n");
        System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

    }
}