package model.data_structures;

import java.util.Iterator;

public class DirectedGraph<K extends Comparable<K>,V> {
	
	private int E;
	private int V;
	private SCHT<K,Vertex> vertices;
	
	public DirectedGraph()
	{
		E=0;
		vertices = new SCHT<>(51);
	}
	
	public class Edge {
	
	private K fuente;
	private K destino;
	private double peso;
	
	public Edge(K fuen, K dest, double pes)
	{
		fuente = fuen;
		destino =dest;
		peso=pes;
	}
	
	public K fuente()	{		return fuente;	}
	
	public K destino()	{		return destino;	}
	
	public double peso()	{		return peso;	}
	}
	
	public class Vertex 
	{
		private K id;
		private V info;
		private DoubleLinkedList<Edge> arcos;
		private boolean marcado;
		
		public Vertex(K ident, V informa)
		{
			id=ident;
			info=informa;
			marcado=false;
		}
		
		public K id() {return id;}
		public V info(){ return info;}
		public DoubleLinkedList<Edge> arcos(){return arcos;}
		public boolean marcado()		{			return marcado;		}

	}


public void addVertex(K id, V info)
{
	Vertex nuevo = new Vertex(id, info);
	vertices.put(id, nuevo);
}

public void addEdge(K fuente, K destino, double peso)
{
	Edge nuevo = new Edge(fuente, destino, peso);
	vertices.get(fuente).arcos().add(nuevo);
	E++;
}

public int E()
{
return E;	
}

public SCHT<K, Vertex> vertices()
{
	return vertices;
}

public DoubleLinkedList<K> adj(K v)
{
	DoubleLinkedList<Edge> arcos =vertices.get(v).arcos;
	DoubleLinkedList<K> res = new DoubleLinkedList<K>();
	Iterator<Edge> iter = arcos.iterator();
	while(iter.hasNext())
	{
		Edge act = iter.next();
		K dest = act.destino();
		res.add(dest);
	}
	return res;
	
}



}
