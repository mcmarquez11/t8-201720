package model.data_structures;

import java.util.Iterator;

public class DirectedCycle<K extends Comparable<K>,V> {

	SCHT<K,Boolean> marcado;
	SCHT<K, Boolean> onStack;
	SCHT<K, K> conectadoA;
	Stack<K> ciclo;
	
	public DirectedCycle(DirectedGraph<K,V> G)
	{
		DoubleLinkedList<K> vertices=G.vertices().Keys;
		
		marcado = new SCHT<>(vertices.getSize());
		onStack = new SCHT<>(vertices.getSize());
		conectadoA = new SCHT<>(vertices.getSize());
		
		Iterator<K> iter = vertices.iterator();
		while(iter.hasNext())
		{
			K act = iter.next();
			if(marcado.get(act)) dsf(G,act);
		}
		
	}
	
	public void dsf(DirectedGraph<K,V> G, K v)
	{
		onStack.put(v, true);
		marcado.put(v, true);
		DoubleLinkedList<K> adj= G.adj(v);
		Iterator<K> iter = adj.iterator();
		while(iter.hasNext())
		{
			K act = iter.next();
			if(hasCycle()) return;
			if(!marcado.get(act))
			{
				conectadoA.put(v, act);
			}
			if(onStack.get(act))
			{
				ciclo = new Stack<K>();
				for (K x=v; x!=act; x=conectadoA.get(x))
				{
				ciclo.push(x);	
				}
				ciclo.push(act);
				ciclo.push(v);
			}
		}
		onStack.put(v, false);	
	}
	
	public boolean hasCycle ()
	{
		return ciclo !=null;
	}
	
	public Stack<K> ciclo()
	{
		return ciclo;
	}
	
}
