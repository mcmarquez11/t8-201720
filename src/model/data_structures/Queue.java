package model.data_structures;


public class Queue<E> implements IQueue<E> {

	private int size=0;
	private Node  First;
	private Node  Last;

	private class Node{
	E Object;
	Node next;
	private Node (E object)
	{		 Object=object;}
	}
		
	
	public void enqueue(E item) {
	Node a= new Node(item);
	if (size==0)
	{First=a;
	Last=a;
	 First.next=a;
	 }
	else{
	
	Last.next=a;
	Last=a;
	
	}
	size ++;	
	}

	@Override
	public E dequeue() {
	E obj= First.Object;
	First=First.next;
	if(size==0)
	{
		Last=null;
	}	
	
	
	size--;
	return obj;
	}
	public int Size()
	{return size;}
		

}
