package model.data_structures;

public interface Graph<K,V> {
	
	public void addEdge(K orig, K dest, int peso);
	
	public void deleteEdge(K orig, K dest);
	
	public void addVertex(K id, V obj);
	
	public void deleteVertex(K id);
	
	public boolean hasPathTo(K orig, K dest);
	
	public Stack<K> pathTo(K orig,K dest );

	
	
	
}
