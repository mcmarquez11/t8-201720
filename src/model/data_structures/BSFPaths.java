package model.data_structures;

import java.util.Iterator;

public class BSFPaths<K extends Comparable<K>,V> {
	
	SCHT<K,Boolean> marcado;
	SCHT<K, K> conectadoA;
	private K f;
	
	public BSFPaths(DirectedGraph<K,V> G, K v)
	{
		marcado= new SCHT<>(G.vertices().size());
		conectadoA = new SCHT<>(G.vertices().size());
		f=v;
		bsf(G,f);
	}
	
	private void bsf(DirectedGraph<K,V> G, K v)
	{
		Queue<K> cola = new Queue<>();
		marcado.put(v, true);
		cola.enqueue(v);
		while(cola.Size()!=0)
		{
			K n = cola.dequeue();
			DoubleLinkedList<K> adj = G.adj(n);
			Iterator<K> iter = adj.iterator();
			while(iter.hasNext())
			{
				K act = iter.next();
				if(!marcado.get(act))
				{
					conectadoA.put(act, n);
					marcado.put(act, true);
					cola.enqueue(act);
				}
			}
		}
	}
	
	public boolean hasPathTo( K v)
	{
		return marcado.get(v);
	}
	
	public Stack<K> pathTo( K v)
	{
		if(!hasPathTo(v)) return null;
		Stack<K> camino = new Stack<K>();
		for (K x=v; x!=f; x=conectadoA.get(x))
		{
			camino.push(x);			
		}
		camino.push(f);
		return camino;
	}
}
