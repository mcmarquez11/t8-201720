package model.data_structures;

import java.util.Iterator;

import Test.SeparateChainingHashTest;

public class UnGraph <K, V> implements Graph<K,V>{

	private int E;
	private SCHT<K,Vertex> vertices;
	private SCHT<K, Boolean> marcado;
	private SCHT<K,K> edgeTo;
	
	
	public UnGraph(int v)
	{
		E=0;
		vertices= new SCHT<>(v);
	}
	
	
	private class Edge {
	
		public K dest;
		public int peso;
		
		public Edge(K destino, int pes)
		{
			dest= destino;
			peso=pes;
		}
	}
	
	
	private class Vertex {
		
		public K id;
		public V objeto;
		private SCHT<K, Edge> adj;
		
		public Vertex(K ident, V obj)
		{
			objeto = obj;
			id=ident;
			adj= new SCHT<>(31);
		}
		
		public SCHT<K, Edge> adjList() {
			return adj;
		}
	}

		
	public void addEdge(K orig, K dest, int peso) {
		
		if(vertices.contains(orig) && vertices.contains(dest))
		{
			SCHT<K, Edge> adyOr= vertices.get(orig).adjList();
			SCHT<K, Edge> adyDes= vertices.get(dest).adjList();

			if(!adyOr.contains(dest) && !adyDes.contains(orig))
			{
				Edge newOr = new Edge(dest, peso);
				Edge newDes = new Edge(orig, peso);
				adyOr.put(dest, newOr);
				adyDes.put(orig, newDes);
				E++;
			}
			else System.out.println("Ya existe un arco de "+ orig+ " a "+ dest);
		}
		else System.out.println("Alguno de los vertices "+orig+ " o "+dest+" no existe.");
	}


	
	public void deleteEdge(K orig, K dest) {
		
		SCHT<K, Edge> adyOr= vertices.get(orig).adjList();
		SCHT<K, Edge> adyDes= vertices.get(dest).adjList();
		if(adyOr.contains(dest) && adyDes.contains(orig))
		{
			adyOr.Delete(dest);
			adyDes.Delete(orig);
		}
		else System.out.println("El arco entre "+orig+ " y "+dest+" no existe.");

	}

	
	public void addVertex(K id, V obj) {
		
		if(!vertices.contains(id))
		{
			Vertex nuevo = new Vertex(id, obj);
			vertices.put(id, nuevo);
		}
		else System.out.println("Ya existe un v�rtice con id "+id);
	}

	
	
	//Borra un v�rtice y todos los arcos insidentes a �l
	public void deleteVertex(K id) {
		
		DoubleLinkedList<K> vert= vertices.Keys;
		Iterator<K> iter= vert.iterator();
		while(iter.hasNext())
		{
			Vertex vertAct =vertices.get(iter.next());
			if(vertAct.adjList().contains(id))
				{
				deleteEdge(id, vertAct.id);
				}
		}
		vertices.Delete(id);

	}

	
	
	private void lookPath(K orig, K dest)
	{
		marcado = new SCHT<>(V());
		edgeTo= new SCHT<>(V());		
		DoubleLinkedList<K> ver= vertices.Keys;
		Iterator<K> iter = ver.iterator();
		while(iter.hasNext())
		{
			K act= iter.next();
			marcado.put(act, false);
			edgeTo.put(act, null);
		}

		dsf(orig, orig);
		
	}
	private void dsf(K orig, K dest)
	{
		marcado.put(dest, true);
		SCHT<K,Edge> ady= vertices.get(dest).adjList();
		DoubleLinkedList<K> arcos =ady.Keys;
		Iterator<K> iter = arcos.iterator();
		while(iter.hasNext())
		{
			
		}
	}
	
	public boolean hasPathTo(K orig, K dest) {
		// TODO Auto-generated method stub
		return false;
	}


	// retorna null si no hay camino etre orig y dest
	public Stack<K> pathTo(K orig, K dest) {
		// TODO Auto-generated method stub
		return null;
	}

	public int E()
	{
		return E;
	}
	
	public int V()
	{
		return vertices.size();
	}

	
	
}
