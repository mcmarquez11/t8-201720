package model.logic;

import api.ISTSManager;
import jdk.nashorn.internal.runtime.arrays.IteratorAction;
import model.vo.BusUpdateVO;
import model.vo.VORoute;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Scanner;

import model.vo.VOStop;
import model.vo.VOStopTime;
import model.vo.VOTrip;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.LPHT;
import model.data_structures.SCHT;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.org.apache.xml.internal.utils.Trie;
import com.sun.prism.paint.Stop;

public class STSManager implements ISTSManager  {
// la muestra de la carga de archivos cvc se muestra en LoadRoutes. La carga de archivos JSON se muestra en LoadBusUpdate
	
	static String ruta="./data/Static";
	static String ruta1="./data/RealTime";
	public String fecha;
   
	private File archivo;
	

	private LPHT<Integer, VORoute>Routes= new LPHT<>(250);
	private SCHT<Integer, VOStop>Stops;
	private SCHT<Integer, VOTrip>Trips= new SCHT<>(60000);
    private SCHT<Integer,SCHT<Integer, VOTrip>> Trips1=new SCHT<>(250);
	private DoubleLinkedList<BusUpdateVO> BUpdate=new DoubleLinkedList<BusUpdateVO>();
    private SCHT<Integer,SCHT<Integer, VOStopTime>> StopTimes1=new SCHT<>(1500000);
    private SCHT<Integer,SCHT<Integer,VOStopTime>> StopTimesS;
    private SCHT<Integer, VOTrip>TripsConRetardo=new SCHT<>(6000);
	private SCHT<Integer, DoubleLinkedList<BusUpdateVO>> reportesTrip =new SCHT<>(1100000);
    
	public void ITScargarGTFS() throws IOException
	{
		loadRoutes(ruta+"/routes.txt");
		loadTrips(ruta+"/trips.txt");
//		loadAgency(ruta+"/agency.txt");
		loadStops(ruta+"/stops.txt");
//		loadCalendar(ruta+"/calendar.txt");
//		loadCalendarDates(ruta+"/calendar_dates.txt");
//		loadTransfers(ruta+"/transfers.txt");
		loadStopTimes(ruta+"/stop_times.txt");
	}

   
	// Muestra comos e cargan los archivos JSON
	public void readBusUpdate(File rtFile) throws FileNotFoundException {
		
		BufferedReader reader = null;
	   
	        reader = new BufferedReader(new FileReader(rtFile));
	        Gson gson = new GsonBuilder().create();
	   
          	        BusUpdateVO[] buses = gson.fromJson(reader, BusUpdateVO[].class);
	        	    
	       for(int i=0;i<buses.length;i++)
          	      //for(int i=0;i<1000;i++)
	        {
	       
	        BusUpdateVO bus=buses[i];
	        int tripId = bus.TripId();
	        if(!reportesTrip.contains(tripId))
	        {
	        	DoubleLinkedList<BusUpdateVO> rep = new DoubleLinkedList<>();
	        	reportesTrip.put(tripId, rep);
	        }
	        DoubleLinkedList<BusUpdateVO> reportes = reportesTrip.get(tripId);
	        reportes.add(bus);
	        
	        BUpdate.add(buses[i]);
	        
	      // System.out.println("$$LAT  " +buses[i].TripId());
	        }
	}
	
	
	//Muestra como se cargan los Archivos cvc
	public void loadRoutes(String routesFile) throws IOException {
	
		
		archivo = new File (routesFile);
		if (archivo==null)
		{
			throw new IOException("El archivo que se intenta abrir es nulo");
		}
		else
		{
		FileReader reader = new FileReader (archivo);
		
		BufferedReader lector = new BufferedReader(reader);
		String Linea=lector.readLine();
		Linea=lector.readLine();
		while (Linea!=null)
		{
			String[] a= Linea.split(",");
			int id=Integer.parseInt(a[0]);
			String Agency=a[1];
		    String shortName=a[2];
			String longname=a[3];
	
			VORoute Route= new VORoute(id, longname, shortName, Agency);
			Routes.put(id, Route);
		
			Linea=lector.readLine();
		}
		lector.close();
		reader.close();
		}
		System.out.println("Total rutas: "+Routes.keys().getSize());
	}

	public void loadTrips(String tripsFile) throws IOException
		{  
		DoubleLinkedList<Integer> kk=(DoubleLinkedList<Integer>)Routes.keys();
		  Iterator<Integer> it=kk.iterator();
				while(it.hasNext())
				
				{  int key=it.next();
				    SCHT<Integer, VOTrip> tt= new SCHT<>(150);
				    Trips1.put(key, tt);
				}
				int cont=0;
			archivo = new File (tripsFile);
			if (archivo==null)
			{			throw new IOException("El archivo que se intenta abrir es nulo");
			}
			else
			{
			FileReader reader = new FileReader (archivo);
			
			BufferedReader lector = new BufferedReader(reader);
			String Linea=lector.readLine();
			Linea=lector.readLine();
			String[] b= Linea.split(",");
	
			while (Linea!=null)
			{
							
				String[] a= Linea.split(",");
				int RouteId=Integer.parseInt(a[0]);
				int tripId=Integer.parseInt(a[2]);
				int service=Integer.parseInt(a[1]);
				int shape=Integer.parseInt(a[7]);		
			    String Name=a[3];
			   	VOTrip tt= new VOTrip(RouteId, service, shape, tripId, Name);		
			    Trips.put(tripId,tt);
			    SCHT<Integer, VOTrip> TT=Trips1.get(RouteId);
			    TT.put(tripId,tt);
			   //System.out.println(StopTimes1.get(tripId).get(stopSequence).DepartureTime()+"   "+StopTimes1.get(tripId).get(stopSequence).TripId());
			    cont++;
			    Linea=lector.readLine();
			}lector.close();
		}
//			System.out.println("El tama�o de LOS TRIPS es de "+cont);
			System.out.println("Total Trips: "+Trips.size());
			System.out.println("Total Trips1 keys (Rutas): "+ Trips1.keys().getSize());
		}

	
	public void loadAgency(String File) throws IOException {
				
		archivo = new File (File);
		if (archivo==null)
		{			throw new IOException("El archivo que se intenta abrir es nulo");
		}
		else
		{
		FileReader reader = new FileReader (archivo);
		
		BufferedReader lector = new BufferedReader(reader);
		String Linea=lector.readLine();
		Linea=lector.readLine();
		while (Linea!=null)
		{
			String[] a= Linea.split(",");
	
			String idd=a[0];
		    String namee=a[1];
		    
		    Linea=lector.readLine();
		}
	}}


	//public void loadCalendarDates(String File) throws IOException {
		// TODO Auto-generated method stub
		
	//}

	public void loadStops(String stopsFile) throws IOException {
		Stops = new SCHT<>(9000);
		archivo = new File (stopsFile);
		if (archivo==null)
		{			throw new IOException("El archivo que se intenta abrir es nulo");
		}
		else
		{
		FileReader reader = new FileReader (archivo);
		
		BufferedReader lector = new BufferedReader(reader);
		String Linea=lector.readLine();
		Linea=lector.readLine();
		while (Linea!=null)
		{
			String[] a= Linea.split(",");
			
			int Id =0;
			int code = 0;
			if(a[0].equals(" "))
			{
				System.out.println(a[2] +" no tiene id");
			}
			else if(a[0].equals(" ")==false)
			{
				Id=Integer.parseInt(a[0]);
			}
			else if(a[1].equals(" "))
			{
				System.out.println(a[2] +" no tiene c�digo");
			}
			else
			{
				code=Integer.parseInt(a[1]);
			}
				
		    String name=a[2];
			double lat=Double.parseDouble(a[4]);
			double lon=Double.parseDouble(a[5]);
		    String zoneId=a[6];

		    VOStop stop= new VOStop(Id, code, name, lat, lon, zoneId);
		   Stops.put(Id, stop);

		
		// System.out.println( "STOP"+ Stops.get(Id).Lat());
		    Linea=lector.readLine();
		}
		System.out.println("Total Stops: "+Stops.size());
		}}

	public void loadStopTimes(String stopTimesFile) throws IOException {
			StopTimesS = new SCHT<>(50003);

			int cont=0;
		archivo = new File (stopTimesFile);
		if (archivo==null)
		{			throw new IOException("El archivo que se intenta abrir es nulo");
		}
		else
		{
		FileReader reader = new FileReader (archivo);
		
		BufferedReader lector = new BufferedReader(reader);
		String Linea=lector.readLine();
		Linea=lector.readLine();
		String[] b= Linea.split(",");
		int trip=Integer.parseInt(b[0]);
		SCHT<Integer, VOStopTime>H1=new SCHT<Integer,VOStopTime>(50);
		
		StopTimes1.put(trip, H1);
		while (Linea!=null)
		{		
			String[] a= Linea.split(",");
	
			int tripId=Integer.parseInt(a[0]);
		
		    String arrTime=a[1];
		    String depTime=a[2];
			int stopId=Integer.parseInt(a[3]);
		
			int stopSequence=Integer.parseInt(a[4]);
			double distTraveled=0;
			if(a.length==7)
			{
				distTraveled=Double.parseDouble(a[8]);
			}

		    VOStopTime stTime= new VOStopTime(tripId, arrTime, depTime, stopId, stopSequence, distTraveled);
			if(tripId!=trip)
			{trip=tripId;
			 H1=new SCHT<Integer,VOStopTime>(50); 
			 H1.put(stopId,stTime);
			 StopTimes1.put(tripId, H1);
			 }
			else
			{
				H1.put(stopId, stTime);
			}
			if (!StopTimesS.contains(stopId))
			{
				StopTimesS.put(stopId, null);
			}
			if(StopTimesS.get(stopId)==null)
			{SCHT<Integer, VOStopTime> H2=new SCHT<Integer, VOStopTime>(53);
				H2.put(tripId, stTime);
				StopTimesS.put(stopId, H2);
			}
			else
			{SCHT<Integer, VOStopTime> H22=StopTimesS.get(stopId);
			H22.put(tripId, stTime);
			}
		    cont++;
		    Linea=lector.readLine();
		}lector.close();
	}
//		System.out.println("El tama�o del stop es de "+cont);
		System.out.println("Total StopTimes1 keys (key=trips): "+StopTimes1.size());
		System.out.println("StopTimesS solo keys (keys=stops): "+StopTimesS.size());
	}
	
	



	public void prueba(int dia)  
	{
//	 
//		DoubleLinkedList<Integer>key=(DoubleLinkedList<Integer>)Trips1.keys();
//        Iterator<Integer> iter=key.iterator();
//        int cont=0;
//  
//     while(iter.hasNext())
//     {
//     	int kk=iter.next();
//     	
//     SCHT<Integer, VOTrip> tr=Trips1.get(kk);
//     DoubleLinkedList<Integer>ke=(DoubleLinkedList<Integer>)tr.keys();
//     Iterator<Integer>it=ke.iterator();
//     while(it.hasNext())
//     {
//          cont++;
//     	int kk1=it.next();
//     	System.out.println(tr.get(kk1).TripId()+" "+cont);
//     }
//     
//     }

	}
	
	private int service(int dia)
	{
	     int service =0;
 		if(dia==0)		service =3;
			else if(dia==1)	service =1;
			else if(dia==2)	service =1;
			else if(dia==3)	service =1;
			else if(dia==4)	service =1;
			else if(dia==5)	service =1;
			else if(dia==6)	service =2;
 		return service;
	}
	  private Double toRad(Double value) {
	        return value * Math.PI / 180;
	    }
	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
   
        final int R = 6371*1000; 
    
        Double latDistance = toRad(lat2-lat1);
        Double lonDistance = toRad(lon2-lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
                   Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
                   Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        Double distance = R * c;
         
        return Math.abs(distance);
 
    }

	
}





