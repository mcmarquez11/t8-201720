package model.vo;

public class BusUpdateVO {

	int TripId;
	String RouteNo;
	String Destination;
	Double Latitude;
	Double Longitude;
	String RecordedTime;
	
public BusUpdateVO(int tripId, String routeNo,String destination, double lat, double lon, String recordedTime){


	TripId=tripId;
	RouteNo=routeNo;
	Destination=destination;
	Latitude=lat;
	Longitude=lon;
	RecordedTime=recordedTime;
		}
public int TripId()
{return TripId;
	}

public String RouteNo()
{return RouteNo;
	}
public String Destination()
{return Destination;
	}
public double Latitude(){
	return Latitude;
}
public double Longitude(){
	return Longitude;
}
public String RecordedTime()
{return RecordedTime;
	}
}

