package model.vo;

/**
 * Representation of a route object
 */


public class VORoute {

private int id;
private String LongName;
private String Agency;
private String ShortName;	

 public VORoute(int ID, String name,String sshort,String agency ){
	
id=ID;
LongName=name;
Agency=agency;
ShortName=sshort;
	
}
/**
 * @return id - Route's id number
 */
	public int id() {
	
		return id;
		
	}

	/**
	 * @return name - route name
	 */
	public String LongName() {
		return LongName;
	
	}
	public String Agency()
	{return Agency;}
	
	public String ShortName()
	{return ShortName;}
		
		
	
	

}
