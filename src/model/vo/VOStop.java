package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop {
	
	private int Id;
	private int Code;
	private String Name;
	private double Lat;
	private double Lon;
	private String ZoneId;
	
	public VOStop(int id,int code, String name,double lat, double lon,String zoneid){
	
		Id=id;
		Code=code;
		Name=name;
		Lat=lat;
		Lon=lon;
		ZoneId=zoneid;
				}
	
	public int Id() {
			return Id;
	}
	public int Code() {
		return Code;
}
	public String getName() {
	
		return Name;
	}
	public double Lat() {
		return Lat;
}
	public double Lon() {
		return Lon;
}
	public String ZoneId() {
		
		return ZoneId;
	}


}
