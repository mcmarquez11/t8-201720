package api;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.vo.BusUpdateVO;
import model.vo.VORoute;
import model.vo.VOStop;


/**
 * Basic API for testing the functionality of the STS manager
 */
public interface ISTSManager  {

	/**
	 * Method to load the routes of the STS
	 * @param routesFile - path to the file 
	 */
	
	public void ITScargarGTFS() throws IOException;
	
	
	public void readBusUpdate(File file) throws FileNotFoundException;

	
	public  void loadStops(String a)throws IOException ;
	
	
	public void loadStopTimes(String stopTimesFile) throws IOException ;
	

}
